package com.mail.comm.function.choosevideo

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.R
import com.mail.comm.base.BaseAty
import com.mail.comm.function.chooseimg.ChooseImageBean
import com.mail.comm.function.chooseimg.ItemDecoration
import com.mail.comm.image.ImageLoader
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.x
import java.io.File

class ChooseVideoAty :BaseAty(), CommonCallback<List<ChooseVideoBean>> {

    var list = ArrayList<ChooseVideoBean>()
    var mChooseVideoUtil: ChooseVideoUtil? = null

    var relay_top_bg:RelativeLayout?=null
    var tv_title:TextView?=null
    var recyclerview:RecyclerView?=null

    override fun getLayoutId(): Int = R.layout.aty_choose_video

    override fun initView() {
        relay_top_bg =findViewById(R.id.relay_top_bg)
        tv_title =findViewById(R.id.tv_title)
        mChooseVideoUtil = ChooseVideoUtil()
        recyclerview =findViewById(R.id.recyclerview)

    }

    override fun requestData() {
        mChooseVideoUtil?.getLocalVideoList(this)
    }

    override fun callback(bean: List<ChooseVideoBean>?) {
        list.add(ChooseVideoBean(0))
        if (bean!=null&&bean.size>0){
            list.addAll(bean)
        }
        recyclerview?.layoutManager = GridLayoutManager(this, 4)
        var decoration =  ItemDecoration(mContext, 0x00000000, 5F, 5F);
        decoration.setOnlySetItemOffsetsButNoDraw(true);
        recyclerview?.addItemDecoration(decoration)
        var mAdapter = GoldRecyclerAdapter(this)
        recyclerview?.adapter = mAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTopview(relay_top_bg)
        tv_title?.text= "所有视频"
    }


    fun mainClick(v:View){

        when(v.id){
            R.id.relay_back->{
                finish()
            }
        }
    }

    var mVideoPath =""

    fun recordVideo() {
        var dcmi_path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).absolutePath
        var camera_img_path = dcmi_path+"/"+resources.getString(R.string.app_name)+"/video/"
        val dir = File(camera_img_path)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        var mVideoFile = File(dir, System.currentTimeMillis().toString() + ".mp4")
        mVideoPath = mVideoFile.absolutePath
        val intent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        var uri: Uri?
        if (Build.VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile(this@ChooseVideoAty,
                x.app().packageName+".FileProvider", mVideoFile)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        } else {
            uri = Uri.fromFile(mVideoFile)
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 15)
        startActivityForResult(intent, 101)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode!= RESULT_OK)return

        when(requestCode){

            101 ->{
                var intent = Intent()
                intent.putExtra("type","video_record")
                intent.putExtra("data",mVideoPath)
                setResult(RESULT_OK,intent)
                finish()
            }

            104 ->{
                var path = data?.getStringExtra("data")
                var intent = Intent()
                intent.putExtra("type","video")
                intent.putExtra("data",path)
                setResult(RESULT_OK,intent)
                finish()
            }
        }

    }

    inner class GoldRecyclerAdapter(context: Context?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            when (viewType) {
                0 -> {
                    return fGoldViewHolder(inflater.inflate(R.layout.item_choose_video_01, parent, false))
                }
                else -> {
                    return fGoldViewHolder2(inflater.inflate(R.layout.item_choose_video, parent, false))
                }
            }
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int = position

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


            if (holder is fGoldViewHolder2) {
                ImageLoader.loadVideoThumb(this@ChooseVideoAty,list[position].videoFile,holder.imgv)
                holder.tv_time?.text = list[position].durationString
            }

            holder.itemView.setOnClickListener {

                if (getItemViewType(position)==0){
                    recordVideo()
                }else{
                    var bundle = Bundle()
                    bundle.putString("video_path",list[position].videoFile.absolutePath)
                    startActivityForResult(ChooseVideoPreviewAty::class.java,bundle,104)
                }
            }
        }

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            init {
                AutoUtils.autoSize(itemView)
            }
        }

        inner class fGoldViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv: ImageView? = null
            var tv_time: TextView? = null
            init {
                AutoUtils.autoSize(itemView)
                imgv = itemView.findViewById(R.id.imgv)
                tv_time = itemView.findViewById(R.id.tv_time)
            }
        }
    }


}