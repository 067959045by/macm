package com.mail.comm.function.audio;

import android.annotation.SuppressLint;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder.AudioSource;
import android.os.Environment;


import com.mail.comm.R;
import com.zhy.autolayout.utils.L;

import org.xutils.x;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;


@SuppressLint("NewApi")
public class AudioRecorderEx {

    public static final String AUDIO_SUFFIX_MP3 = ".mp3";
    private List<String> mRecordFiles = new ArrayList<String>();
    private String savePath;
    private final static int[] sampleRates = {44100, 22050, 11025, 8000};
    private static final int TIMER_INTERVAL = 120;
    private AudioRecord audioRecorder = null;
    private int cAmplitude = 0;
    public State state;
    private RandomAccessFile randomAccessWriter;
    private short nChannels;
    private int sRate;
    private short bSamples;
    private int bufferSize;
    private int aSource;
    private int aFormat;
    private int framePeriod;
    private byte[] buffer;
    private int payloadSize;
    private int lastCamplitude = 0;
    public enum State {
        INITIALIZING, READY, RECORDING, ERROR, STOPPED
    }

    public static AudioRecorderEx getInstance() {
        AudioRecorderEx result = new AudioRecorderEx(AudioSource.MIC,
                sampleRates[3], AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        return result;
    }


    public State getState() {
        return state;
    }

    private AudioRecord.OnRecordPositionUpdateListener updateListener = new AudioRecord.OnRecordPositionUpdateListener() {

        public void onPeriodicNotification(AudioRecord recorder) {

            new Thread(new Runnable() {
                @Override
                public void run() {
                    writeData();
                }
            }).start();
        }

        public void onMarkerReached(AudioRecord recorder) {
        }
    };

    private synchronized void writeData() {
        if (state == State.STOPPED) {
            return;
        }

        audioRecorder.read(buffer, 0, buffer.length); // Fill buffer
        try {
            randomAccessWriter.write(buffer); // Write buffer to file
            payloadSize += buffer.length;
            L.e("buffer size:" + buffer.length + " payloadSize:" + payloadSize);
            if (bSamples == 16) {
                for (int i = 0; i < buffer.length / 2; i++) { // 16bit
                    short curSample = getShort(buffer[i * 2],
                            buffer[i * 2 + 1]);
                    if (curSample > cAmplitude) { // Check amplitude
                        cAmplitude = curSample;
                    }
                }
            } else { // 8bit sample size
                for (int i = 0; i < buffer.length; i++) {
                    if (buffer[i] > cAmplitude) { // Check amplitude
                        cAmplitude = buffer[i];
                    }
                }
            }
        } catch (Exception e) {
            L.e("Error occured in updateListener, recording is aborted");
        }
    }

    public AudioRecorderEx(int audioSource,
                           int sampleRate, int channelConfig, int audioFormat) {
        try {
            if (audioFormat == AudioFormat.ENCODING_PCM_16BIT) {
                bSamples = 16;
            } else {
                bSamples = 8;
            }

            if (channelConfig == AudioFormat.CHANNEL_IN_MONO) {
                nChannels = 1;
            } else {
                nChannels = 2;
            }

            aSource = audioSource;
            sRate = sampleRate;
            aFormat = audioFormat;

            framePeriod = sampleRate * TIMER_INTERVAL / 1000;
            bufferSize = framePeriod * 2 * bSamples * nChannels / 8;
            if (bufferSize < AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat)) { // Check to make sure
                bufferSize = AudioRecord.getMinBufferSize(sampleRate, channelConfig, audioFormat);
                framePeriod = bufferSize / (2 * bSamples * nChannels / 8);
                L.e("Increasing buffer size to " + Integer.toString(bufferSize));
            }

            audioRecorder = new AudioRecord(audioSource, sampleRate, channelConfig, audioFormat, bufferSize);

            if (audioRecorder.getState() != AudioRecord.STATE_INITIALIZED)
                throw new Exception("AudioRecord initialization failed");
            audioRecorder.setRecordPositionUpdateListener(updateListener);
            audioRecorder.setPositionNotificationPeriod(framePeriod);

            cAmplitude = 0;
            savePath = null;
            state = State.INITIALIZING;
        } catch (Exception e) {
            if (e.getMessage() != null) {
                L.e(e.getMessage());
            } else {
                L.e("Unknown error occured while initializing recording");
            }
            state = State.ERROR;
        }
    }

    public void setOutputFile(String argPath) {

        try {
            if (state == State.INITIALIZING) {
                this.savePath = argPath;
            }
        } catch (Exception e) {
            if (e.getMessage() != null) {
                L.e(e.getMessage());
            } else {
                L.e("Unknown error occured while setting output path");
            }
            state = State.ERROR;
        }
    }

    public int getMaxAmplitude() {
        if (state == State.RECORDING) {
            int result = cAmplitude;

            if (result == 0) {
                result = lastCamplitude;
            }
            int volume = 0;
            if (result > 28000) {
                volume = result / 400;
            } else if (result <= 28000 && result > 2000) {
                volume = result / 327;
            } else if (result > 327 && result <= 2000) {
                volume = result / 150;
            } else {
                volume = result / 50;
            }
            lastCamplitude = cAmplitude;
            cAmplitude = 0;
            return volume;
        } else {
            return 0;
        }
    }

    public void prepare() {
        try {
            if (state == State.INITIALIZING) {

                if ((audioRecorder.getState() == AudioRecord.STATE_INITIALIZED) & (savePath != null)) {

                    randomAccessWriter = new RandomAccessFile(savePath, "rw");
                    randomAccessWriter.setLength(0); // Set file length to
                    randomAccessWriter.writeBytes("RIFF");
                    randomAccessWriter.writeInt(0); // Final file size not
                    randomAccessWriter.writeBytes("WAVE");
                    randomAccessWriter.writeBytes("fmt ");
                    randomAccessWriter.writeInt(Integer.reverseBytes(16)); // Sub-chunk
                    randomAccessWriter.writeShort(Short.reverseBytes((short) 1)); // AudioFormat, 1 for
                    randomAccessWriter.writeShort(Short.reverseBytes(nChannels));// Number of channels,
                    randomAccessWriter.writeInt(Integer.reverseBytes(sRate)); // Sample
                    randomAccessWriter.writeInt(Integer.reverseBytes(sRate * bSamples * nChannels / 8)); // Byte rate,
                    randomAccessWriter.writeShort(Short.reverseBytes((short) (nChannels * bSamples / 8))); // Block
                    randomAccessWriter.writeShort(Short.reverseBytes(bSamples)); // Bits per sample
                    randomAccessWriter.writeBytes("data");
                    randomAccessWriter.writeInt(0); // Data chunk size not

                    L.e("fileheader length:" + randomAccessWriter.length());
                    buffer = new byte[framePeriod * bSamples / 8 * nChannels];
                    state = State.READY;
                } else {
                    L.e("prepare() method called on uninitialized recorder");
                    state = State.ERROR;
                }
            } else {
                L.e("prepare() method called on illegal state");
                release();
                state = State.ERROR;
            }
        } catch (Exception e) {
            if (e.getMessage() != null) {
                L.e(e.getMessage());
            } else {
                L.e("Unknown error occured in prepare()");
            }
            state = State.ERROR;
        }
    }

    public void release() {
        if (state == State.RECORDING) {
            stop();
        } else {
            if (state == State.READY) {
                try {
                    randomAccessWriter.close(); // Remove prepared file
                } catch (IOException e) {
                    L.e("I/O exception occured while closing output file");
                }
                (new File(savePath)).delete();
            }
        }
        if (audioRecorder != null) {
            audioRecorder.release();
        }
    }

    public void reset() {
        try {
            if (state != State.ERROR) {
                savePath = null; // Reset file path
                cAmplitude = 0; // Reset amplitude
                state = State.INITIALIZING;
            }
        } catch (Exception e) {
            L.e(e.getMessage());
            state = State.ERROR;
        }
    }

    public void clearData() {
        if (mRecordFiles != null && mRecordFiles.size() != 0) {
            mRecordFiles.clear();
        }
    }

    public void start() {
        if (state == State.READY) {
            payloadSize = 0;
            audioRecorder.startRecording();
            audioRecorder.read(buffer, 0, buffer.length);
            state = State.RECORDING;
            mRecordFiles.add(savePath);
        } else {
            L.e("start() called on illegal state");
            state = State.ERROR;
        }
    }

    public synchronized void stop() {
        if (state == State.RECORDING) {
            audioRecorder.stop();
            L.e("payloadSize:" + payloadSize);
            try {
                randomAccessWriter.seek(4); // Write size to RIFF header
                randomAccessWriter.writeInt(Integer.reverseBytes(36 + payloadSize));
                randomAccessWriter.seek(40); // Write size to Subchunk2Size
                randomAccessWriter.writeInt(Integer.reverseBytes(payloadSize));
                randomAccessWriter.close();
            } catch (IOException e) {
                L.e("I/O exception occured while closing output file");

                state = State.ERROR;
            }
            state = State.STOPPED;
        } else {
            L.e("stop() called on illegal state");
            state = State.ERROR;
        }
    }


    public void finishRecord() {
        if (state != State.RECORDING) {
            return;
        }
        stop();
    }

    private short getShort(byte argB1, byte argB2) {
        return (short) (argB1 | (argB2 << 8));
    }


    public File mergeAudioFile() {

        String dcmi_path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
        String music_path = dcmi_path+"/"+ x.app().getResources().getString(R.string.app_name)+"/music/";
        File mixFile = new File(music_path, System.currentTimeMillis()+".mp3");
        String path = mixFile.getAbsolutePath();
        if (mRecordFiles.size() == 0) {
            return mixFile;
        }
        if (mixFile.exists()) {
            RandomAccessFile dscAudioFile = null;
            try {
                dscAudioFile = new RandomAccessFile(path, "rw");
                for (String filePath : mRecordFiles) {

                    dscAudioFile.seek(mixFile.length());
                    RandomAccessFile itemFile = new RandomAccessFile(filePath, "rw");
                    itemFile.seek(44);
                    byte[] b = new byte[1024 * 5];
                    int len;
                    while ((len = itemFile.read(b)) != -1) {
                        dscAudioFile.write(b, 0, len);
                    }
                    dscAudioFile.seek(4);
                    dscAudioFile.writeInt(Integer.reverseBytes(36  + ((int) dscAudioFile.length()) - 44));
                    dscAudioFile.seek(40);
                    dscAudioFile.writeInt(Integer.reverseBytes(((int) dscAudioFile.length()) - 44));
                    itemFile.close();
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    dscAudioFile.close();
                } catch (Exception e) {
                }
            }
        } else {
            try {
//                //如果合成文件还不存在，则把第一个需要合并的文件重命名为要合并生成的文件
                File file = new File(mRecordFiles.get(0));
                if (!file.exists()) {
                    return mixFile;
                }
                file.renameTo(mixFile);
                //从第二个文件开始合并文件
                RandomAccessFile dscAudioFile = new RandomAccessFile(path, "rw");
                for (int i = 1; i < mRecordFiles.size(); i++) {
                    dscAudioFile.seek(dscAudioFile.length());
                    RandomAccessFile itemFile = new RandomAccessFile(mRecordFiles.get(i), "rw");
                    itemFile.seek(44);
                    byte[] b = new byte[1024 * 5];
                    int len;
                    while ((len = itemFile.read(b)) != -1) {
                        dscAudioFile.write(b, 0, len);
                    }
                    dscAudioFile.seek(4);
                    dscAudioFile.writeInt(Integer.reverseBytes(36/* + ((int) itemFile.length()) - 44)*/ +
                            ((int) dscAudioFile.length()) - 44));
                    dscAudioFile.seek(40);
                    dscAudioFile.writeInt(Integer.reverseBytes(/*((int) itemFile.length()) - 44) +*/
                            ((int) dscAudioFile.length()) - 44));
                    itemFile.close();
                }
                dscAudioFile.close();

                if (file.exists()) {
                    file.delete();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        deleteListRecord();
        mRecordFiles.clear();
        if (mixFile.exists()) {
            L.e("file size:" + mixFile.length());
        }
        mRecordFiles.add(mixFile.getAbsolutePath());
        return mixFile;
    }


    /**
     * 上传完成后删除本地录音文件
     * @param path
     */
    public void deleteMixRecorderFile(String path) {
        File audioDir = new File(path);
        if (audioDir.exists() && audioDir.isDirectory()) {
            File[] files = audioDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (!files[i].getName().endsWith(AudioRecorderEx.AUDIO_SUFFIX_MP3)) {
                    files[i].delete();
                }
            }
        }
    }

    public void deleteMixRecorderFile(File audioDir) {
        if (audioDir != null) {
            if (audioDir.exists() && audioDir.isDirectory()) {
                File[] files = audioDir.listFiles();
                for (int i = 0; i < files.length; i++) {
                    if (!files[i].getName().endsWith(AudioRecorderEx.AUDIO_SUFFIX_MP3)) {
                        files[i].delete();
                    }
                }
            }
        }

    }


    public void deleteRecorderFileUnLessSpec(String path, String specFilePath) {
        File audioDir = new File(path);
        if (audioDir.exists() && audioDir.isDirectory()) {
            File[] files = audioDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (!files[i].getPath().equals(specFilePath)) {
                    files[i].delete();
                }
            }
        }
    }

    public void deleteListRecord() {
        for (String fileName : mRecordFiles) {
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
            }
        }
    }

}
