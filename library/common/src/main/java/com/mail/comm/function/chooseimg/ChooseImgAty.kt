package com.mail.comm.function.chooseimg

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.R
import com.mail.comm.base.BaseAty
import com.mail.comm.image.ImageLoader
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.x
import java.io.File

class ChooseImgAty :BaseAty(), CommonCallback<List<ChooseImageBean>> {

    var max_num =9
    var mCameraPath = ""//拍照后得到的图片路径
    var list = ArrayList<ChooseImageBean>()
    var list_check = ArrayList<String>()

    var tv_title:TextView?=null
    var tv_right:TextView?=null
    var relay_top_bg:RelativeLayout?=null
    var recyclerview:RecyclerView?=null

    var chooseImageUtil:ChooseImageUtil? = null

    override fun getLayoutId(): Int = R.layout.aty_choose_img

    override fun initView() {
        relay_top_bg =findViewById(R.id.relay_top_bg)
        tv_title =findViewById(R.id.tv_title)
        tv_right =findViewById(R.id.tv_right)
        recyclerview =findViewById(R.id.recyclerview)
        chooseImageUtil = ChooseImageUtil()
        max_num =intent.getIntExtra("max_num",9)
    }

    override fun requestData() {
        chooseImageUtil?.getLocalImageList(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTopview(relay_top_bg)
        tv_title?.text= "所有图片"
        tv_right?.text = "完成"
        tv_right?.visibility = View.VISIBLE
    }

    fun mainClick(v:View){

        when(v.id){
            R.id.relay_back->{
                finish()
            }
            R.id.tv_right ->{
                if (list_check.size==0){
                    showToastS("请选择图片")
                    return
                }
                var intent = Intent()
                intent.putExtra("type","photo")
                intent.putStringArrayListExtra("data",list_check)
                setResult(RESULT_OK,intent)
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        chooseImageUtil?.release()
    }

    override fun callback(bean: List<ChooseImageBean>?) {
        list.add(ChooseImageBean(0))
        if (bean!=null&&bean.size>0){
            list.addAll(bean)
        }
        recyclerview?.layoutManager = GridLayoutManager(this, 4)
        var decoration =  ItemDecoration(mContext, 0x00000000, 5F, 5F);
        decoration.setOnlySetItemOffsetsButNoDraw(true);
        recyclerview?.addItemDecoration(decoration)
        var mAdapter = GoldRecyclerAdapter(this)
//        mAdapter?.setHasStableIds(true)
        recyclerview?.adapter = mAdapter
    }

    inner class GoldRecyclerAdapter(context: Context?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        val inflater: LayoutInflater = LayoutInflater.from(context)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            when (viewType) {
                0 -> {
                    return fGoldViewHolder2(inflater.inflate(R.layout.item_choose_img_camera, parent, false))
                }
                else -> {
                    return fGoldViewHolder(inflater.inflate(R.layout.item_choose_img, parent, false))
                }
            }
        }

        override fun getItemCount(): Int = list.size

        override fun getItemViewType(position: Int): Int = position

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {

                ImageLoader.loadImage(this@ChooseImgAty, list[position].imageFile.toString(), holder.imgv)

                if (list[position].isChecked){
                    holder.imgv_check?.setImageResource(R.drawable.icon_select_1)
                }else{
                    holder.imgv_check?.setImageResource(R.drawable.icon_select_0)
                }

                holder.itemView.setOnClickListener {

                    if (!list[position].isChecked&&list_check.size==max_num){
                        showToastL("最多选择${max_num}张图片")
                        return@setOnClickListener
                    }

                    list[position].isChecked = !list[position].isChecked

                    if (list[position].isChecked){
                        list_check.add(list[position].imageFile.absolutePath)
                    }else{
                        list_check.remove(list[position].imageFile.absolutePath)
                    }

                    tv_right?.text = "完成("+list_check.size+"/"+max_num+")"
                    notifyItemChanged(position)
                }
            }

            if (holder is fGoldViewHolder2) {
                holder.itemView.setOnClickListener {
                    takePhoto()
                }
            }
        }

        inner class fGoldViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var imgv: ImageView? = null
            var imgv_check: ImageView? = null
            init {
                AutoUtils.autoSize(itemView)
                imgv = itemView.findViewById(R.id.imgv)
                imgv_check = itemView.findViewById(R.id.imgv_check)
            }
        }

        inner class fGoldViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {
            init {
                AutoUtils.autoSize(itemView)
            }
        }
    }
    
     fun takePhoto() {

        var dcmi_path =Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).absolutePath
        var camera_img_path = dcmi_path+"/"+resources.getString(R.string.app_name)+"/camera/"
        val dir = File(camera_img_path)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        var mCameraFile = File(dir, System.currentTimeMillis().toString() + ".png")
        mCameraPath = mCameraFile.absolutePath
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        var uri: Uri?
        if (Build.VERSION.SDK_INT >= 24) {
            uri = FileProvider.getUriForFile(this@ChooseImgAty,x.app().packageName+".FileProvider", mCameraFile)
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        } else {
            uri = Uri.fromFile(mCameraFile)
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, 101)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode== RESULT_OK&&requestCode==101){
            var intent = Intent()
            intent.putExtra("type","camera")
            intent.putExtra("data",mCameraPath)
            setResult(RESULT_OK,intent)
            finish()
        }

    }

}