package com.mail.comm.view.refresh


interface XRefreshInterface {
    fun refreshStart()
    fun loadMoreStart()
}