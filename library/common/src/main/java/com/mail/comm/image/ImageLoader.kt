package com.mail.comm.image

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.TextUtils
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.zhy.autolayout.utils.AutoUtils
import java.io.File

class ImageLoader {

    companion object {

        fun loadImage(context: Context?, url: String?, imgv: ImageView?) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImage
            }
            Glide.with(context)
                .load(url)
                .into(imgv)
        }

        fun loadImage(context: Context?, url: String?, imgv: ImageView?, error: Int, placeholder: Int) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImage
            }
            val requestOptions = RequestOptions()
            requestOptions.error(error)
            requestOptions.placeholder(placeholder)
            Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(imgv)
        }

        fun loadImageCircle(context: Context?, url: String?, imgv: ImageView?) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImageCircle
            }
            val requestOptions = RequestOptions()
            requestOptions.apply(RequestOptions.bitmapTransform(CircleCrop()))
            Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(imgv)
        }

        fun loadImageCircle2(context: Context?, url: String, imgv: ImageView?) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImageCircle2
            }
            val requestOptions = RequestOptions()
            requestOptions.transform(GlideCircleTransform(context))
            Glide.with(context)
                .load(url)
                .apply(requestOptions)
                .into(imgv)
        }

        fun loadImageRadius(context: Context, url: String?, imgv: ImageView?,radius:Int=20) {
            if (context == null || TextUtils.isEmpty(url) || imgv == null) {
                return@loadImageRadius
            }
            val transformation = CornerTransform(
                context,
                AutoUtils.getPercentWidthSize(radius).toFloat()
            )
            val requestOptions = RequestOptions()
            requestOptions.transform(transformation)
            Glide.with(context)
                .load(url).apply(requestOptions)
                .into(imgv)
        }


        fun loadImageDrawable(context: Context?, url: String?, callback: DrawableCallback?) {
            if (context == null || TextUtils.isEmpty(url) ) {
                return@loadImageDrawable
            }
            Glide.with(context)
                .asDrawable()
                .load(url)
                .into(object : SimpleTarget<Drawable?>() {

                    override fun onLoadFailed( errorDrawable: Drawable?) {
                        callback?.onLoadFailed()
                    }

                    override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable?>?) {
                        callback?.onLoadSuccess(resource)
                    }
                })

        }

        /**
         * 显示视频封面缩略图
         */
        fun loadVideoThumb(context: Context?, file: File?, imageView: ImageView?) {
            if (context == null) {
                return
            }
            try {
                Glide.with(context)
                    .asDrawable()
                    .load(Uri.fromFile(file))
                    .skipMemoryCache(false)
                    .into(imageView!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    interface DrawableCallback {
        fun onLoadSuccess(drawable: Drawable?)
        fun onLoadFailed()
    }

}