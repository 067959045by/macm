package com.mail.comm.view.refresh

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.RelativeLayout
import com.mail.comm.R
import com.zhy.autolayout.utils.AutoUtils
import me.dkzwm.widget.srl.SmoothRefreshLayout
import me.dkzwm.widget.srl.extra.IRefreshView
import me.dkzwm.widget.srl.indicator.IIndicator

class XRefreshHeaderView<T : IIndicator?> : RelativeLayout, IRefreshView<T> {

    var imageViewLoading: ImageView? = null
    var imageViewLoadingAnimation: Animation?=null

    constructor(context: Context?) : this(context, null)

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {

        imageViewLoadingAnimation = AnimationUtils.loadAnimation(context, R.anim.loading_dialog_rotate)
        imageViewLoadingAnimation?.interpolator = LinearInterpolator()
        createView(this)
    }

    override fun getType(): Int = IRefreshView.TYPE_HEADER.toInt()

    override fun getStyle(): Int = IRefreshView.STYLE_DEFAULT.toInt()

    override fun getCustomHeight(): Int =0

    override fun getView(): View =this

    private fun createView(layout: RelativeLayout) {
        imageViewLoading = ImageView(layout.context)
        imageViewLoading?.scaleType = ImageView.ScaleType.FIT_XY
        imageViewLoading?.setImageResource(R.drawable.ic_loading_tip)
        val imgSize = AutoUtils.getPercentWidthSize(60)
        val layoutParams = LayoutParams(imgSize, imgSize)
        layoutParams.addRule(CENTER_IN_PARENT)
        val bottomMargin = AutoUtils.getPercentWidthSize(180)
        layoutParams.setMargins(0, 0, 0, bottomMargin)
        layout.addView(imageViewLoading, layoutParams)
    }

    /**
     * 手指离开屏幕;
     */
    override fun onFingerUp(layout: SmoothRefreshLayout, indicator: T) {}

    /**
     * 重置视图;
     */
    override fun onReset(layout: SmoothRefreshLayout) {}

    /**
     * 重新配置视图，准备刷新;
     */
    override fun onRefreshPrepare(layout: SmoothRefreshLayout) {}

    /**
     * 开始刷新;
     */
    override fun onRefreshBegin(layout: SmoothRefreshLayout, indicator: T) {
        imageViewLoading?.startAnimation(imageViewLoadingAnimation)
    }

    /**
     * 刷新完成;
     */
    override fun onRefreshComplete(layout: SmoothRefreshLayout, isSuccessful: Boolean) {
        if (imageViewLoading != null) {
            imageViewLoading!!.clearAnimation()
        }
    }

    /**
     * 当头部或者尾部视图发生位置变化;
     */
    override fun onRefreshPositionChanged(
        layout: SmoothRefreshLayout,
        status: Byte,
        indicator: T
    ) {
    }

    /**
     * 当头部或者尾部视图仍然处于处理事务中，这时候移动其他刷新视图则会调用该方法;
     * 在1.4.6版本新加入;
     */
    override fun onPureScrollPositionChanged(
        layout: SmoothRefreshLayout,
        status: Byte,
        indicator: T
    ) {
    }


}