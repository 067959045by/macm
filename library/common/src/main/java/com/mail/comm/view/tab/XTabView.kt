package com.mail.comm.view.tab

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.mail.comm.R
import com.zhy.autolayout.config.AutoLayoutConifg
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.util.LogUtil

class XTabView : LinearLayout {

    private var mContext: Context? = null
    private var mScale = 0f
    private var mTip: String? = null
    private var mIconSize = 0
    private var mTextSize = 0
    private var mTextColorChecked = 0
    private var mTextColorUnChecked = 0
    private var mChecked = false
    private var mImg: ImageView? = null
    private var mText: TextView? = null
    private var mAnimator: ValueAnimator? = null
    private var iconResArray = ArrayList<Int>()

    constructor(context: Context?) : this(context, null)

    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : this(context, attrs, defStyleAttr, 0)

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {

        mContext = context
        mScale = context!!.resources.displayMetrics.density
        val ta = context?.obtainStyledAttributes(attrs, R.styleable.TabButton)
        val iconArrayId = ta.getResourceId(R.styleable.TabButton_tbn_icon_array_id, 0)
        mTip = ta.getString(R.styleable.TabButton_tbn_tip)
        mIconSize = ta.getDimension(R.styleable.TabButton_tbn_icon_size, 0f).toInt()
        mTextSize = ta.getDimension(R.styleable.TabButton_tbn_text_size, 0f).toInt()
        mTextColorChecked = ta.getColor(R.styleable.TabButton_tbn_text_color_checked, 0)
        mTextColorUnChecked = ta.getColor(R.styleable.TabButton_tbn_text_color_unchecked, 0)
        mChecked = ta.getBoolean(R.styleable.TabButton_tbn_checked, false)
        ta.recycle()
        mIconSize = AutoUtils.getPercentWidthSize(mIconSize)
        if (iconArrayId != 0) {
            val arr = resources.obtainTypedArray(iconArrayId)
            val len = arr.length()
            for (i in 0 until len) {
                iconResArray.add(arr.getResourceId(i, 0))
            }
            arr.recycle()
        }

        mAnimator = ValueAnimator.ofFloat(1f, (iconResArray.size - 1).toFloat())
        mAnimator?.addUpdateListener { animation ->
            val v = animation.animatedValue as Float
            val index = v.toInt()
            mImg?.setImageResource(iconResArray[index])
        }
        mAnimator?.setDuration(300)
        mAnimator?.interpolator = AccelerateDecelerateInterpolator()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        orientation = VERTICAL
        gravity = Gravity.CENTER
        mImg = ImageView(mContext)
        val params1 = LayoutParams(mIconSize, mIconSize)
        params1.setMargins(0, dp2px(4), 0, 0)
        mImg?.layoutParams = params1
        mImg?.scaleType =ImageView.ScaleType.FIT_XY
        if (mChecked) {
            mImg?.setImageResource(iconResArray[iconResArray.size - 1])
        } else {
            mImg?.setImageResource(iconResArray[0])
        }

        mText = TextView(mContext)
        val params2 = LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        mText?.layoutParams = params2
        mText?.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize.toFloat())
        mText?.text = mTip
        mText?.setTextColor(if (mChecked) mTextColorChecked else mTextColorUnChecked)

        addView(mImg)
        addView(mText)
    }

    fun setChecked(checked: Boolean) {
        mChecked = checked
        if (mChecked) {
            mText?.setTextColor(mTextColorChecked)
            mAnimator?.start()
        } else {
            mAnimator?.cancel()
            mImg?.setImageResource(iconResArray[0])
            mText?.setTextColor(mTextColorUnChecked)
        }
    }


     fun dp2px(dpVal: Int): Int {
        return (mScale * dpVal + 0.5f).toInt()
    }

}