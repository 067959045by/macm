package com.mail.comm.view.refresh

import android.content.Context
import android.util.AttributeSet
import com.zhy.autolayout.utils.AutoUtils
import me.dkzwm.widget.srl.RefreshingListenerAdapter
import me.dkzwm.widget.srl.SmoothRefreshLayout
import me.dkzwm.widget.srl.extra.footer.MaterialFooter
import me.dkzwm.widget.srl.extra.header.ClassicHeader
import me.dkzwm.widget.srl.extra.header.MaterialHeader
import me.dkzwm.widget.srl.indicator.DefaultIndicator
import me.dkzwm.widget.srl.indicator.IIndicator
import me.dkzwm.widget.srl.util.PixelUtl

class XSmoothRefreshLayout : SmoothRefreshLayout {

    var refreshAndLoadListen: XRefreshInterface? = null

    constructor(context: Context) : this(context,null)

    constructor(context: Context, attrs: AttributeSet?) : this(context,attrs,0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    fun setXRefreshAndLoadListen(refreshAndLoadListen: XRefreshInterface?) {
        this.refreshAndLoadListen = refreshAndLoadListen
    }

    fun setEnableRefresh(f: Boolean) = setDisableRefresh(!f)

    fun setEnableLoadmore(f: Boolean) = setDisableLoadMore(!f)

    fun startRefresh() = autoRefresh(true)

    fun setIsPinContentView(s: Boolean) = setEnablePinContentView(s)

    fun finishLoadmore() = refreshComplete(500)

    fun finishRefreshing() =  refreshComplete(500)

    private fun init(context: Context) {
//        val src = intArrayOf(-0x1000000)
//        val header: MaterialHeader<*> = MaterialHeader<DefaultIndicator>(context)
//        header.setColorSchemeColors(src)
//        header.setPadding(0, PixelUtl.dp2px(context, 40f), 0, PixelUtl.dp2px(context, 20f))
        setHeaderView(XRefreshHeaderView<DefaultIndicator>(context))
        setEnablePullToRefresh(false)
        setDisableLoadMore(true)
//        val footer: MaterialFooter<*> = MaterialFooter<DefaultIndicator>(context)
//        footer.setProgressBarColors(src)
//        footer.setProgressBarWidth(5)
//        footer.setProgressBarRadius(AutoUtils.getPercentWidthSize(20))
        setFooterView(XRefreshFootView<DefaultIndicator>(context))
//        setIndicatorOffsetCalculator { status, currentPos, offset ->
//            if (status.toFloat() == IIndicator.DEFAULT_RATIO_TO_REFRESH) {
//                if (offset < 0) {
//                    offset
//                } else Math.pow(Math.pow((currentPos / 2).toDouble(), 1.28) + offset, 1 / 1.28)
//                    .toFloat() * 2 - currentPos
//            } else if (status.toFloat() == IIndicator.DEFAULT_RATIO_TO_REFRESH) {
//                if (offset > 0) {
//                    offset
//                } else -(Math.pow(Math.pow((currentPos / 2).toDouble(), 1.28) - offset, 1 / 1.28)
//                    .toFloat() * 2 - currentPos)
//            } else {
//                if (offset > 0) {
//                    Math.pow(offset.toDouble(), 1 / 1.28).toFloat() * 2
//                } else if (offset < 0) {
//                    (-Math.pow(-offset.toDouble(), 1 / 1.28)).toFloat() * 2
//                } else {
//                    offset
//                }
//            }
//        }
        setOnRefreshListener(object : RefreshingListenerAdapter() {
            override fun onRefreshing() {
                if (refreshAndLoadListen != null) {
                    refreshAndLoadListen!!.refreshStart()
                }
            }

            override fun onLoadingMore() {
                if (refreshAndLoadListen != null) {
                    refreshAndLoadListen!!.loadMoreStart()
                }
            }
        })
    }

    fun loadMoreReturn() {
        setDisableLoadMore(false)
        //        setDisablePerformRefresh(true);
        setDisablePerformLoadMore(true)
        footerView?.view?.visibility = GONE
    }

    fun loadMoreReturn2() {
        setDisableLoadMore(false)
        setDisablePerformRefresh(true)
        setDisablePerformLoadMore(true)
        setEnableKeepRefreshView(false)
        headerView?.view?.visibility = GONE
        footerView?.view?.visibility = GONE
    }
}