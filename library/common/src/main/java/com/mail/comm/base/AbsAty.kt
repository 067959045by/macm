package com.mail.comm.base

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.widget.RelativeLayout
import com.mail.comm.interfaces.LifeCycleListener
import com.mail.comm.net.ApiListener
import com.zhy.autolayout.AutoLayoutActivity
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.common.Callback
import org.xutils.http.RequestParams
import kotlin.collections.ArrayList

open class AbsAty : AutoLayoutActivity(), ApiListener {

    var mContext: Context? = null

    var mLifeCycleListeners: ArrayList<LifeCycleListener>? = null

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = this
        mLifeCycleListeners = ArrayList()
        for (listener in mLifeCycleListeners!!) {
            listener.onCreate()
        }
    }

    override fun onDestroy() {
        for (listener in mLifeCycleListeners!!) {
            listener.onDestroy()
        }
        mLifeCycleListeners?.clear()
        mLifeCycleListeners = null
        super.onDestroy()
    }

    override fun onStart() {
        super.onStart()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onStart()
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onReStart()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onResume()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onPause()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (mLifeCycleListeners != null) {
            for (listener in mLifeCycleListeners!!) {
                listener.onStop()
            }
        }
    }

    fun addLifeCycleListener(listener: LifeCycleListener?) {
        if (mLifeCycleListeners != null && listener != null) {
            mLifeCycleListeners!!.add(listener)
        }
    }

    fun addAllLifeCycleListener(listeners: ArrayList<LifeCycleListener>?) {
        if (mLifeCycleListeners != null && listeners != null) {
            mLifeCycleListeners!!.addAll(listeners)
        }
    }

    fun removeLifeCycleListener(listener: LifeCycleListener?) {
        if (mLifeCycleListeners != null) {
            mLifeCycleListeners!!.remove(listener!!)
        }
    }


    override fun onCancelled(var1: Callback.CancelledException?) {}

    override fun onComplete(var1: RequestParams?, var2: String?, type: String?) {
    }

    override fun onError(var1: Map<String?, String?>?, var2: RequestParams?) {
    }

    override fun onExceptionType(var1: Throwable?, params: RequestParams?, type: String?) {
    }


    //非沉淀状态栏   背景-资源文件
    fun initTopview(relay: RelativeLayout?,bgColor:String = "#ffffff") {
        val lp = relay?.layoutParams as RelativeLayout.LayoutParams
        lp.height = AutoUtils.getPercentHeightSize(130)
        relay.layoutParams = lp
        relay.setBackgroundColor(Color.parseColor(bgColor))
    }

}


