package com.mail.myapplication.ui.jetpack.mvvm.net;


import com.mail.myapplication.ui.jetpack.mvvm.model.WeatherEntity;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiService {

    // http://api.k780.com/?app=weather.today&weaid=1&appkey=10003&sign=b59bc3ef6191eb9f747dd4e83c99f2a4&format=json
    @GET("/")
    Observable<BaseResponse<WeatherEntity>> getWeatherInfo(@QueryMap Map<String, String> map);
}
