package com.mail.myapplication.ui.jetpack.mvvm.viewmodel;
import android.util.Log;
import androidx.lifecycle.ViewModel;

import com.mail.comm.net.ApiListener;

import org.jetbrains.annotations.Nullable;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;

import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel extends ViewModel implements ApiListener {

    protected final CompositeDisposable compositeDisposable;

    public BaseViewModel() {
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        Log.i("zee", "onCleared: ");
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    public void onCancelled(@Nullable Callback.CancelledException var1) {

    }

    @Override
    public void onComplete(@Nullable RequestParams var1, @Nullable String var2, @Nullable String type) {

    }

    @Override
    public void onError(@Nullable Map<String, String> var1, @Nullable RequestParams var2) {

    }

    @Override
    public void onExceptionType(@Nullable Throwable var1, @Nullable RequestParams params, @Nullable String type) {

    }
}
