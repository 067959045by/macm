package com.mail.myapplication.ui.utils;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class PermissonUtils {

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };


    public static PermissonUtils with() {
        PermissonUtils permissonUtils = new PermissonUtils();
        return permissonUtils;
    }

    private List<String> mPermissionList = new ArrayList<>();

    private PermissionListen permissionListen;

    public PermissonUtils initPermisson(Activity context) {
        mPermissionList.clear();
        for (int i = 0; i < PERMISSIONS_STORAGE.length; i++) {
            if (ContextCompat.checkSelfPermission(context, PERMISSIONS_STORAGE[i]) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(PERMISSIONS_STORAGE[i]);
            }
        }
        if (mPermissionList.isEmpty() || mPermissionList.size() == 0 || Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            if (permissionListen!=null){
                permissionListen.onSuccess();
            }
        } else {
            String[] permissions = mPermissionList.toArray(new String[mPermissionList.size()]);//将List转为数组
            ActivityCompat.requestPermissions(context, permissions, 101);
        }
        return  this;
    }

    public PermissonUtils setPermissionsListen(PermissionListen permissionListen) {
        this.permissionListen = permissionListen;
        return PermissonUtils.this;
    }

    public interface PermissionListen {
        void onSuccess();
    }
}
