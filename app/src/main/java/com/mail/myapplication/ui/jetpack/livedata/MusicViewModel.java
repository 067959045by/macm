package com.mail.myapplication.ui.jetpack.livedata;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import java.util.ArrayList;
import java.util.List;


/**
 * 处理数据的变化跟视图
 */
public class MusicViewModel extends ViewModel {

    // 定义一个对象，相当于用来存放数据的仓库
    private static MutableLiveData<List<String>> liveData;

    /**
     * 用于获取数据
     * 并保存到LiveData中(可以理解成数据仓库)
     *
     * @return
     */
    public MutableLiveData<List<String>> getMusics() {
        if (liveData == null) {
            List<String> musics = loadMusics();
            liveData = new MutableLiveData<>();
            //liveData.setValue(musics);
            // 把数据存放到仓库
            // post 和 set 是跟同步异步区别
            liveData.postValue(musics);
        }
        return liveData;

    }

    /**
     * 加载数据
     * 可以从本地或者通过服务端请求获得
     * @return
     */
    private List<String> loadMusics() {
//        List<String> musics = DBManager.getInstance().getBaseDao(Music.class).query(null);
        List<String> musics = new ArrayList<>();
        musics.add("001");
        musics.add("002");
        musics.add("003");
        return musics;
    }


    /**
     * 修改某条数据
     *
     * @param index
     * @param music
     */
    public void changeItem(int index, String music) {
        List<String> musics = liveData.getValue();
        musics.set(index, music);
        liveData.postValue(musics);
    }

}
