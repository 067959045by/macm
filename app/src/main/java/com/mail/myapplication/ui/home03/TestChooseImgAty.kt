package com.mail.myapplication.ui.home03

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.mail.comm.function.chooseimg.ChooseImagePreviewAty
import com.mail.comm.function.chooseimg.ChooseImgAty
import com.mail.comm.image.ImageLoader
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import java.lang.StringBuilder

class TestChooseImgAty :BaseXAty(){

    val CHOOSE_IMG = 100
    var relay_top_bg:RelativeLayout?=null
    var tv_title:TextView?=null
    var edit_01:EditText?=null
    var imgv_01:ImageView?=null
    var listImg = ArrayList<String>()
    override fun getLayoutId(): Int = R.layout.aty_chooseimg_test

    override fun initView() {

        relay_top_bg =findViewById(R.id.relay_top_bg)
        tv_title =findViewById(R.id.tv_title)
        edit_01 =findViewById(R.id.edit_01)
        imgv_01 =findViewById(R.id.imgv_01)

    }

    override fun requestData() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTopview(relay_top_bg)
        tv_title?.text= "test choose img"
    }

    fun mainClick(v:View){

        when(v.id){

            R.id.relay_back->{
                finish()
            }

            R.id.btn_01 ->{
                var bundle = Bundle()
                bundle.putInt("max_num",6)
                startActivityForResult(ChooseImgAty::class.java,bundle,CHOOSE_IMG)
            }

            R.id.btn_02 ->{
                var bundle = Bundle()
                bundle.putStringArrayList("list",listImg)
                startActivity(ChooseImagePreviewAty::class.java,bundle)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK){
            when(requestCode){

                100 ->{
                    var type =data?.getStringExtra("type")

                    when (type) {
                        "photo" -> {

                            var list = data?.getStringArrayListExtra("data")

                            if (list!=null){
                                listImg.clear()
                                listImg.addAll(list)
                            }

                            var stringBuilder = StringBuilder()

                            for (str in list!!) {
                                stringBuilder.append(str)
                                stringBuilder.append("\n")
                            }

                            edit_01?.setText(stringBuilder.toString())
                        }
                        "camera" -> {
                            var cameraImgPath = data?.getStringExtra("data")
                            edit_01?.setText(cameraImgPath)
                            ImageLoader.loadImage(this@TestChooseImgAty,cameraImgPath,imgv_01)
                        }
                    }


                }
            }

        }
    }



}