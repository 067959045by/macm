package com.mail.myapplication.ui.jetpack.mvvm.viewmodel;


import androidx.databinding.ObservableField;

import com.mail.myapplication.interfaces.Home;

import org.jetbrains.annotations.Nullable;
import org.xutils.http.RequestParams;


public class WeatherViewModel extends BaseViewModel {

    public ObservableField<String> date = new ObservableField<>();
    public ObservableField<String> temp = new ObservableField<>();
    public ObservableField<String> city = new ObservableField<>();
    public ObservableField<String> weather = new ObservableField<>();
    private Home home;

    //从网络获取数据
    public void refreshData() {
        if (home==null){
            home = new Home();
        }
        home.getHomeData(this);
    }

    @Override
    public void onComplete(@Nullable RequestParams var1, @Nullable String var2, @Nullable String type) {
        super.onComplete(var1, var2, type);
        date.set("2028");
        city.set("北京");
    }

    @Override
    public void onExceptionType(@Nullable Throwable var1, @Nullable RequestParams params, @Nullable String type) {
        super.onExceptionType(var1, params, type);
    }
}
