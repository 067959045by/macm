package com.mail.myapplication.ui.jetpack.livedata

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyTestLivedatabus2Binding


class TestLiveDataBus2Aty : BaseXAty() {

    override fun getLayoutId(): Int = 0

    override fun initView() {}

    override fun requestData() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var mBinding = AtyTestLivedatabus2Binding.inflate(layoutInflater);
        setContentView(mBinding?.root)
        testLiveDataBus()
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }
            R.id.btn_00 -> {
                LiveDataBus.getInstance().with("test01",String::class.java)
                    .setValue("002")
            }
        }
    }



    fun testLiveDataBus(){

        LiveDataBus.getInstance().with("test01",String::class.java)
            .observe(this, Observer<String>() {s->
                Toast.makeText(this, s, Toast.LENGTH_SHORT).show()
            })

    }


}