package com.mail.myapplication.ui.web

import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.just.agentweb.AgentWeb
import com.just.agentweb.WebChromeClient
import com.just.agentweb.WebViewClient
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R

class WebAty : BaseXAty() {

    var relay_top_bg: RelativeLayout? = null
    var tv_title: TextView? = null
    var linlay_01: LinearLayout? = null
    var mAgentWeb: AgentWeb? = null

    override fun getLayoutId(): Int = R.layout.aty_web

    override fun initView() {

        relay_top_bg = findViewById(R.id.relay_top_bg)
        tv_title = findViewById(R.id.tv_title)
        linlay_01 = findViewById(R.id.linlay_01)

        mAgentWeb = AgentWeb.with(this).setAgentWebParent(linlay_01!!, LinearLayout.LayoutParams(-1, -1)) //传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams ,第一个参数和第二个参数应该对应。
            .useDefaultIndicator() // 使用默认进度条
            .setWebChromeClient(mWebChromeClient)
            .setWebViewClient(mWebViewClient)
            .createAgentWeb()
            .ready()
            .go("http://chuniao.me")
    }

    override fun requestData() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTopview(relay_top_bg)
        tv_title?.text = "test"
    }

    override fun onPause() {
        mAgentWeb!!.webLifeCycle.onPause()
        super.onPause()
    }

    override fun onBackPressed() {
        if (!mAgentWeb!!.back()) {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        mAgentWeb!!.webLifeCycle.onResume()
        super.onResume()
    }

    override fun onDestroy() {
        mAgentWeb!!.webLifeCycle.onDestroy()
        super.onDestroy()
    }


    val mWebChromeClient: WebChromeClient = object : WebChromeClient() {
//        override fun onProgressChanged(view: WebView, newProgress: Int) {
//
//        }
    }

    var mWebViewClient: WebViewClient = object : WebViewClient() {
//        override fun onPageStarted(view: WebView, url: String, favicon: Bitmap) {
//
//        }
    }

    fun mainClick(v: View) {

        when (v.id) {
            R.id.relay_back -> {
                finish()
            }
        }

    }
}