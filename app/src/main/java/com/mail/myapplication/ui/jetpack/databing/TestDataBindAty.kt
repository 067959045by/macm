package com.mail.myapplication.ui.jetpack.databing

import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyTestDatabingBinding


class TestDataBindAty : BaseXAty() {

    override fun getLayoutId(): Int = 0

    override fun initView() {}

    override fun requestData() {}

    var weatherEntity: WeatherEntity?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var mBinding = AtyTestDatabingBinding.inflate(layoutInflater)
//        var mBinding = DataBindingUtil.setContentView<AtyTestDatabingBinding>(this,R.layout.aty_test_databing)
        setContentView(mBinding?.root)
//      mBinding.setVariable(BR.days,)
        mBinding.activity = this
        weatherEntity = WeatherEntity()
        weatherEntity?.citynm = "北京";
        weatherEntity?.temp_curr = "-12℃";
        weatherEntity?.days = "2021-01-29";
        mBinding.weather = weatherEntity
    }

    fun mainClick(v: View) {

        when (v.id) {
            R.id.relay_back -> {
                showToastS("dddddddll")
                weatherEntity?.days = "2021-01-33ll"
            }
        }
    }

    fun onClick(v: View) {
        showToastS("test001")
    }

}