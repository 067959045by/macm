package com.mail.myapplication.ui.jetpack.mvvm.view;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.mail.myapplication.databinding.ActivityWeatherBinding;
import com.mail.myapplication.ui.jetpack.mvvm.BaseActivity;
import com.mail.myapplication.ui.jetpack.mvvm.viewmodel.WeatherViewModel;

// 编译之后打包dex之前进行修改的WeatherActivity的父类 -> Hilt_WeatherActivity
public class TestMvvmAty extends BaseActivity<ActivityWeatherBinding, WeatherViewModel> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initViews() {
        viewModel.refreshData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("zee", "onDestroy: ");
    }

}
