package com.mail.myapplication.ui.home03

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.R
import com.mail.myapplication.anim.ViewAnim01Holder
import com.mail.myapplication.ui.Main2Aty
import com.mail.myapplication.ui.jetpack.databing.TestDataBindAty
import com.mail.myapplication.ui.jetpack.livedata.TestLiveDataBusAty
import com.mail.myapplication.ui.jetpack.mvvm.view.TestMvvmAty
import com.mail.myapplication.ui.web.WebAty

class Home03Frg : BaseXFrg(), View.OnClickListener {

    var btn_01: Button? = null
    var btn_02: Button? = null
    var btn_03: Button? = null
    var btn_04: Button? = null
    var btn_05: Button? = null
    var btn_06: Button? = null
    var btn_07: Button? = null
    var btn_08: Button? = null
    var mOutView: ViewGroup?=null

    override fun getLayoutId(): Int = R.layout.frg_home03

    override fun initView() {
        mOutView =rootView!!.findViewById(R.id.rootview)
    }

    override fun requestData() {
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_01 = rootView?.findViewById(R.id.btn_01)
        btn_02 = rootView?.findViewById(R.id.btn_02)
        btn_03 = rootView?.findViewById(R.id.btn_03)
        btn_04 = rootView?.findViewById(R.id.btn_04)
        btn_05 = rootView?.findViewById(R.id.btn_05)
        btn_06 = rootView?.findViewById(R.id.btn_06)
        btn_07 = rootView?.findViewById(R.id.btn_07)
        btn_08 = rootView?.findViewById(R.id.btn_08)
        btn_01?.setOnClickListener(this)
        btn_02?.setOnClickListener(this)
        btn_03?.setOnClickListener(this)
        btn_04?.setOnClickListener(this)
        btn_05?.setOnClickListener(this)
        btn_06?.setOnClickListener(this)
        btn_07?.setOnClickListener(this)
        btn_08?.setOnClickListener(this)

    }


    override fun onClick(v: View) {
        when(v.id){

            R.id.btn_01->{
                startActivity(WebAty::class.java)
            }

            R.id.btn_02->{
                show01()
            }

            R.id.btn_03->{
                startActivity(Main2Aty::class.java)
            }

            R.id.btn_04->{
                startActivity(TestChooseImgAty::class.java)
            }

            R.id.btn_05->{
                startActivity(TestChooseVideoAty::class.java)
            }

            R.id.btn_06->{
                startActivity(TestLiveDataBusAty::class.java)
            }

            R.id.btn_07->{
                startActivity(TestDataBindAty::class.java)
            }

            R.id.btn_08->{
                startActivity(TestMvvmAty::class.java)
            }
        }
    }


    var bonusViewHolder: ViewAnim01Holder? = null

    fun show01() {
        if (bonusViewHolder == null) {
            bonusViewHolder = ViewAnim01Holder(activity, mOutView);
        }
        bonusViewHolder?.show()
    }

}