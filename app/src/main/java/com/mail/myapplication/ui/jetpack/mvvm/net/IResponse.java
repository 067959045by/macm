package com.mail.myapplication.ui.jetpack.mvvm.net;

public interface IResponse<T> {

    T getData();

    String getMsg();

    String getCode();

    boolean isSuccess();

}
