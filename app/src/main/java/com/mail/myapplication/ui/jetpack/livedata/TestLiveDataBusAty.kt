package com.mail.myapplication.ui.jetpack.livedata

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.databinding.AtyTestLivedatabusBinding
import com.mail.myapplication.ui.jetpack.ZeeLib


class TestLiveDataBusAty : BaseXAty() {

    override fun getLayoutId(): Int = 0

    override fun initView() {}

    override fun requestData() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var mBinding = AtyTestLivedatabusBinding.inflate(layoutInflater)
        setContentView(mBinding?.root);
        testLiveData()
        testLiveDataBus()
        ZeeLib.getInstance().register(this)
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.relay_back -> {
                finish()
            }

            R.id.btn_00 -> {
                LiveDataBus.getInstance().with("test01",String::class.java)
                    .setValue("001")
            }
            R.id.btn_01->{
                var  viewModel =  ViewModelProvider(this).get(MusicViewModel::class.java)
                viewModel.changeItem(0,"009")
            }

            R.id.btn_02->{
                startActivity(TestLiveDataBus2Aty::class.java)
            }
        }
    }


    fun testLiveData(){
       var  viewModel =  ViewModelProvider(this).get(MusicViewModel::class.java)
        viewModel.musics.observe(this,  Observer<List<String>>() { musics ->

            Toast.makeText(this, musics.toString(), Toast.LENGTH_SHORT).show()
        })

    }

    fun testLiveDataBus(){

        LiveDataBus.getInstance().with("test01",String::class.java)
            .observe(this, Observer<String>() {s->
                Toast.makeText(this, s, Toast.LENGTH_SHORT).show()
            })

    }


}