package com.mail.myapplication.ui

import android.os.Bundle
import android.view.View
import com.mail.comm.view.tab.XTabView
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import com.mail.myapplication.ui.home.HomeFrg
import com.mail.myapplication.ui.home02.Home02Frg
import com.mail.myapplication.ui.home03.Home03Frg


class Main2Aty : BaseXAty() {

    var tab_00: XTabView? = null
    var tab_01: XTabView? = null
    var tab_02: XTabView? = null
    var tab_03: XTabView? = null
    var list_btn: Array<XTabView>? = null

    var position: Int = 0

    override fun getLayoutId(): Int = R.layout.aty_main2

    override fun getFragmentContainerId(): Int = R.id.fralay_content

    override fun initView() {
        findViewAllid()
        list_btn = arrayOf(tab_00!!, tab_01!!, tab_02!!, tab_03!!)
    }

    fun findViewAllid() {
        tab_00 = findViewById(R.id.tab_00);
        tab_01 = findViewById(R.id.tab_01);
        tab_02 = findViewById(R.id.tab_02);
        tab_03 = findViewById(R.id.tab_03);
    }

    override fun requestData() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addFragment(HomeFrg::class.java, null)
    }

    fun mainClick(v: View) {

        when (v.id) {

            R.id.tab_00 -> {
                if (position == 0) return
                position = 0
                setSelector(tab_00, list_btn!!);
                addFragment(HomeFrg::class.java, null)
            }

            R.id.tab_01 -> {
                if (position == 1) return
                position = 1
                setSelector(tab_01, list_btn!!);
                addFragment(Home02Frg::class.java, null)
            }

            R.id.tab_02 -> {
                if (position == 2) return
                position = 2
                setSelector(tab_02, list_btn!!);
                addFragment(Home03Frg::class.java, null)
            }

            R.id.tab_03 -> {
                if (position == 3) return
                position = 3
                setSelector(tab_03, list_btn!!);
            }
        }
    }

    private fun setSelector(tabBtn: XTabView?, list_btn: Array<XTabView>) {
        for (i in list_btn.indices) {
            if (tabBtn == list_btn[i]) {
                list_btn[i].setChecked(true)
            } else {
                list_btn[i].setChecked(false)
            }
        }
    }
}