package com.mail.myapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mail.comm.image.ImageLoader
import com.mail.comm.view.load.XLoadTip
import com.mail.comm.view.refresh.XRefreshInterface
import com.mail.comm.view.refresh.XSmoothRefreshLayout
import com.mail.myapplication.BaseXFrg
import com.mail.myapplication.R
import com.mail.myapplication.interfaces.Home
import com.youth.banner.Banner
import com.youth.banner.indicator.CircleIndicator
import com.zhy.autolayout.utils.AutoUtils
import org.xutils.http.RequestParams
import com.mail.myapplication.databinding.ItemHomeListBinding
import com.mail.myapplication.databinding.ItemHomeListHeadBinding


class HomeListFrg : BaseXFrg(), XLoadTip.LoadingTipXReloadCallback, XRefreshInterface {

    var home = Home()
    var type: String = ""
    var list = ArrayList<String>()
    var list_img = ArrayList<HashMap<String, String>>()
    var page =1;

    override fun getLayoutId(): Int = R.layout.frg_home_list

    companion object {
        fun getInstance(type: String?): HomeListFrg {
            var frg = HomeListFrg()
            var bundle = Bundle()
            bundle.putString("type", type)
            frg.arguments = bundle
            return frg
        }
    }

    var loading:XLoadTip?=null
    var refreshLayout: XSmoothRefreshLayout?=null
    var recyclerview: RecyclerView?=null
    var bannerX: Banner<HashMap<String, String>,ImageNetAdapter>?=null

    private fun findViewAllid() {
        loading =rootView?.findViewById(R.id.loading)
        refreshLayout =rootView?.findViewById(R.id.refreshLayout)
        recyclerview =rootView?.findViewById(R.id.recyclerview)
        bannerX =rootView?.findViewById(R.id.bannerX)
    }

    override fun initView() {
        findViewAllid()
        type = arguments?.getString("type").toString()
        for (index in 1!!..20) {
            list.add(index.toString() + type)
        }
        var map = HashMap<String, String>()
        var map1 = HashMap<String, String>()
        var map2 = HashMap<String, String>()
        map.put(
            "url",
            "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3663359702,1992818410&fm=26&gp=0.jpg"
        )
        map1.put(
            "url",
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fup.enterdesk.com%2Fedpic_360_360%2F64%2F25%2F33%2F642533df78c1ce3ca55ddca1f57d8e80.jpg&refer=http%3A%2F%2Fup.enterdesk.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1623757816&t=6a3629b39737befbfe3bb6f303b7ddae"
        )
        map2.put(
            "url",
            "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fattach.bbs.miui.com%2Fforum%2F201408%2F07%2F213601f2xz7usscm2z1mjh.jpg&refer=http%3A%2F%2Fattach.bbs.miui.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1623757816&t=c80b89610c686d4f87db65fd5ba5dd81"
        )
        list_img.add(map)
        list_img.add(map1)
        list_img.add(map2)
    }

    override fun requestData() {
        loading?.setLoadingTip(XLoadTip.LoadStatus.loading)
        home.a(this)
    }

    fun requestDataNoTip() {
        home.a(this)
    }

    override fun reload() {
        page =1
        requestData()
    }

    override fun refreshStart() {
        page=1
//        startProgressDialog()
        requestDataNoTip()
    }

    override fun loadMoreStart() {
        page++
        requestDataNoTip()
    }

    var isEnable =true
    override fun onComplete(var1: RequestParams?, var2: String?, type: String?) {
        super.onComplete(var1, var2, type)
        loading?.setLoadingTip(XLoadTip.LoadStatus.finish)
        stopProgressDialog()
        refreshLayout?.finishRefreshing()
        refreshLayout?.finishLoadmore()

//        if (list.size > 20) {
//            mAdapter?.notifyDataSetChanged()
//        }
        if (page==1){
            list.clear()
            for (index in 1..40) {
                list.add(index.toString() + this.type)
            }
        }else{
            var listmm = ArrayList<String>()
            for (index in list.size!!..list.size + 20) {
                listmm.add(index.toString() + type)
            }
            list.addAll(listmm)
        }

        if (isEnable){
//            isEnable =false
            mAdapter?.notifyDataSetChanged()
        }


    }

    override fun onExceptionType(var1: Throwable?, params: RequestParams?, type: String?) {
        super.onExceptionType(var1, params, type)
        loading?.setLoadingTip(XLoadTip.LoadStatus.error)
        stopProgressDialog()
        refreshLayout?.finishRefreshing()
        refreshLayout?.finishLoadmore()
    }

    var mAdapter: GoldRecyclerAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recyclerview?.layoutManager = GridLayoutManager(activity, 2)
        mAdapter = GoldRecyclerAdapter()
//        mAdapter?.setHasStableIds(true)
        recyclerview?.adapter = mAdapter
        loading?.setLoadingTipXReloadCallback(this)
        refreshLayout?.setEnableRefresh(true)
        refreshLayout?.setEnableLoadmore(true)
        refreshLayout?.setXRefreshAndLoadListen(this)

        var adapter = ImageNetAdapter(list_img)
        bannerX?.adapter = adapter
        bannerX?.addBannerLifecycleObserver(activity)
        bannerX?.indicator = CircleIndicator(activity)
        bannerX?.setOnBannerListener { data, position ->
            var map = data as HashMap<String, String>
            showToastS(position.toString() + "," + map.get("url"))
        }
    }

    inner class GoldRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            when (viewType) {
                0 -> {
                    return fGoldViewHeadHolder(ItemHomeListHeadBinding.inflate(LayoutInflater.from(context)))
                }
                else -> {
                    return fGoldViewHolder(ItemHomeListBinding.inflate(LayoutInflater.from(context)))
                }
            }
        }

        override fun getItemCount(): Int = list.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            if (holder is fGoldViewHolder) {
                with(holder) {
                    with(mBinding) {
                        tvName.text = list[position]
                        var url = "https://nimg.ws.126.net/?url=http%3A%2F%2Fdingyue.ws.126.net%2F2021%2F0114%2Fea09bd73p00qmxkvg008wc000fk00f4m.png&thumbnail=650x2147483647&quality=80&type=jpg"
                        ImageLoader.loadImage(activity, url, imgv)
                        if (position % 2 == 0) {
                            v02.visibility = View.VISIBLE
                            v01.visibility = View.GONE
                        } else {
                            v01.visibility = View.VISIBLE
                            v02.visibility = View.GONE
                        }
                    }
                }
            }

            if (holder is fGoldViewHeadHolder) {
                with(holder) {
                    with(mBinding) {
                        var adapter = ImageNetAdapter(list_img)
                        bannerX.adapter = adapter
                        bannerX.addBannerLifecycleObserver(activity)
                        bannerX.indicator = CircleIndicator(activity)
                        bannerX.setOnBannerListener { data, position ->
                            var map = data as HashMap<String, String>
                            showToastS(position.toString() + "," + map.get("url"))
                        }
                    }
                }
            }
        }

        override fun getItemViewType(position: Int): Int {
           return position
        }

        inner class fGoldViewHolder(binding: ItemHomeListBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemHomeListBinding = binding
            init {
                AutoUtils.autoSize(binding.root)
            }
        }

        inner class fGoldViewHeadHolder(binding: ItemHomeListHeadBinding) : RecyclerView.ViewHolder(binding.root) {
            var mBinding: ItemHomeListHeadBinding = binding
            init {
                AutoUtils.autoSize(binding.root)
            }
        }
    }


}