package com.mail.myapplication.ui.home03

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.mail.comm.function.choosevideo.ChooseVideoAty
import com.mail.comm.image.ImageLoader
import com.mail.myapplication.BaseXAty
import com.mail.myapplication.R
import java.io.File

class TestChooseVideoAty :BaseXAty(){

    var relay_top_bg:RelativeLayout?=null
    var tv_title:TextView?=null
    var edit_01:EditText?=null
    var imgv_01:ImageView?=null

    override fun getLayoutId(): Int = R.layout.aty_choosevideo_test

    override fun initView() {

        relay_top_bg =findViewById(R.id.relay_top_bg)
        tv_title =findViewById(R.id.tv_title)
        edit_01 =findViewById(R.id.edit_01)
        imgv_01 =findViewById(R.id.imgv_01)

    }

    override fun requestData() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTopview(relay_top_bg)
        tv_title?.text= "test choose img"
    }

    fun mainClick(v:View){

        when(v.id){

            R.id.relay_back->{
                finish()
            }

            R.id.btn_01 ->{
                startActivityForResult(ChooseVideoAty::class.java,102)
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK){

            when(requestCode){

                102 ->{
                    var type =data?.getStringExtra("type")

                    when(type){

                        "video_record" ->{
                            var mVideoPath = data?.getStringExtra("data")
                            edit_01?.setText(mVideoPath)
                            ImageLoader.loadVideoThumb(this@TestChooseVideoAty, File(mVideoPath),imgv_01)
                        }

                        "video" ->{
                            var mVideoPath = data?.getStringExtra("data")
                            edit_01?.setText(mVideoPath)
                            ImageLoader.loadVideoThumb(this@TestChooseVideoAty, File(mVideoPath),imgv_01)
                        }
                    }

                }
            }

        }
    }




}