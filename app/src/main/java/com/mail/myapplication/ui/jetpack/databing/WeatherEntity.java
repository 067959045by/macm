package com.mail.myapplication.ui.jetpack.databing;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import com.mail.myapplication.BR;

public class WeatherEntity extends BaseObservable {

    private String citynm;
    private String temp_curr;
    private String days;


    @Bindable
    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
        notifyPropertyChanged(BR.days);
    }

    public String getCitynm() {
        return citynm;
    }

    public void setCitynm(String citynm) {
        this.citynm = citynm;
    }


    public String getTemp_curr() {
        return temp_curr;
    }

    public void setTemp_curr(String temp_curr) {
        this.temp_curr = temp_curr;
    }


}
