package com.mail.myapplication.anim

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.LinearInterpolator
import com.mail.comm.base.AbsViewHolder
import com.mail.comm.utils.MyUtils
import com.mail.myapplication.R

class ViewAnim01Holder(context: Context?, parentView: ViewGroup?) :
    AbsViewHolder(context, parentView) {

    private var mShowAnimator: ObjectAnimator? = null
    private var mHideAnimator: ObjectAnimator? = null
    private var mDp10 = 0
    private var mDp500 = 0
    private var mGroup: View? = null

    override fun getLayoutId(): Int = R.layout.view_anim_01

    init {
        subscribeActivityLifeCycle()
        addToParent()
    }

    @SuppressLint("ObjectAnimatorBinding")
    override fun init() {
        mGroup = findViewById(R.id.group)
        mDp500 = MyUtils.dpTopx(500)
        mShowAnimator = ObjectAnimator.ofFloat(mGroup, "translationX", mDp500.toFloat(), 0f)
        mShowAnimator?.setDuration(1000)
        mShowAnimator?.setInterpolator(LinearInterpolator())
        mShowAnimator?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {

                Handler().postDelayed({
                        mHideAnimator?.setFloatValues(0f, (-mDp10 - mGroup!!.width).toFloat())
                        mHideAnimator?.start()
                }, 2000)
            }
        })

        mDp10 = MyUtils.dpTopx(10)
        mHideAnimator = ObjectAnimator.ofFloat(mGroup, "translationX", 0f)
        mHideAnimator?.setDuration(1000)
        mHideAnimator?.setInterpolator(AccelerateDecelerateInterpolator())
        mHideAnimator?.addUpdateListener { animation ->
            mGroup?.alpha = 1 - animation.animatedFraction
        }

    }

    fun show() {
        mGroup?.alpha = 1f
        mShowAnimator?.start()
    }

    fun clearAnim() {
        mShowAnimator?.cancel()
        mHideAnimator?.cancel()
        mGroup?.translationX = mDp500.toFloat()
    }

    fun release() {
        mShowAnimator?.cancel()
        mShowAnimator?.removeAllListeners()
        mShowAnimator?.removeAllUpdateListeners()
        mHideAnimator?.cancel()
        mHideAnimator?.removeAllListeners()
        mHideAnimator?.removeAllUpdateListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        release()
    }

}